# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Xfce Bot <transifex@xfce.org>, 2018
# Nick Schermer <nick@xfce.org>, 2023
# scootergrisen, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-28 00:48+0200\n"
"PO-Revision-Date: 2018-06-28 22:08+0000\n"
"Last-Translator: scootergrisen, 2023\n"
"Language-Team: Danish (https://app.transifex.com/xfce/teams/16840/da/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: da\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../org.xfce.Catfish.desktop.in.h:1 ../data/ui/CatfishWindow.ui.h:37
#: ../catfish_lib/catfishconfig.py:91
msgid "Catfish File Search"
msgstr "Catfish Filsøgning"

#: ../org.xfce.Catfish.desktop.in.h:2
msgid "File search"
msgstr "Filsøgning"

#: ../org.xfce.Catfish.desktop.in.h:3
msgid "Search the file system"
msgstr "Søg i filsystemet"

#. TRANSLATORS: Search terms to find this application. Do NOT translate or
#. localize the semicolons! The list MUST also end with a semicolon!
#: ../org.xfce.Catfish.desktop.in.h:5
msgid "files;find;locate;lookup;search;"
msgstr "filer;find;opslå;søg;"

#: ../data/ui/CatfishPreferences.ui.h:1
msgid "Catfish Preferences"
msgstr "Catfish-præferencer"

#: ../data/ui/CatfishPreferences.ui.h:2
msgid "_Close"
msgstr "_Luk"

#: ../data/ui/CatfishPreferences.ui.h:3
msgid "Classic (_Titlebar)"
msgstr "Klassisk (_titelinje)"

#: ../data/ui/CatfishPreferences.ui.h:4
msgid "_Modern (CSD)"
msgstr "_Moderne (CSD)"

#: ../data/ui/CatfishPreferences.ui.h:5
msgid "Your new window layout will be applied after restarting Catfish."
msgstr "Dit nye vindueslayout anvendes når Catfish er blevet genstartet."

#: ../data/ui/CatfishPreferences.ui.h:6
msgid "Window Layout"
msgstr "Vindueslayout"

#: ../data/ui/CatfishPreferences.ui.h:7
msgid "Show _hidden files in the results"
msgstr "Vis _skjulte filer i resultaterne"

#: ../data/ui/CatfishPreferences.ui.h:8
msgid "Show filter _sidebar"
msgstr "Vis filter-_sidelinje"

#: ../data/ui/CatfishPreferences.ui.h:9
msgid "Show file sizes in binary format"
msgstr "Vis filstørrelser i binært format"

#: ../data/ui/CatfishPreferences.ui.h:10
msgid "Uncheck to show file size in decimal format"
msgstr "Fravælg for at vise filstørrelser i decimalt format"

#: ../data/ui/CatfishPreferences.ui.h:11
msgid "Display Options"
msgstr "Vis valgmuligheder"

#: ../data/ui/CatfishPreferences.ui.h:12
msgid "Appearance"
msgstr "Udseende"

#: ../data/ui/CatfishPreferences.ui.h:13
msgid "Path"
msgstr "Sti"

#: ../data/ui/CatfishPreferences.ui.h:14
msgid "Add Directory..."
msgstr "Tilføj mappe ..."

#: ../data/ui/CatfishPreferences.ui.h:15
msgid "_Add"
msgstr "_Tilføj"

#: ../data/ui/CatfishPreferences.ui.h:16
msgid "Remove Directory"
msgstr "Fjern mappe"

#: ../data/ui/CatfishPreferences.ui.h:17
msgid "_Remove"
msgstr "_Fjern"

#: ../data/ui/CatfishPreferences.ui.h:18
msgid "Exclude Directories"
msgstr "Udelad mapper"

#: ../data/ui/CatfishPreferences.ui.h:19
msgid "Close the search _window after opening a file"
msgstr "Luk søge_vinduet når en fil er blevet åbnet"

#: ../data/ui/CatfishPreferences.ui.h:20
msgid "Search within compressed files (.zip, .odt, .docx)"
msgstr "Søg i komprimerede filer (.zip, .odt, .docx)"

#: ../data/ui/CatfishPreferences.ui.h:21
msgid "Miscellaneous"
msgstr "Diverse"

#: ../data/ui/CatfishPreferences.ui.h:22
msgid "Advanced"
msgstr "Avanceret"

#: ../data/ui/CatfishWindow.ui.h:1
msgid "Select a Folder"
msgstr "Vælg en mappe"

#: ../data/ui/CatfishWindow.ui.h:2
msgid "Folder not found!"
msgstr "Mappe ikke fundet!"

#. Restore Cancel button
#. Buttons
#: ../data/ui/CatfishWindow.ui.h:3 ../catfish/CatfishWindow.py:797
#: ../catfish_lib/SudoDialog.py:184
msgid "Cancel"
msgstr "Annuller"

#: ../data/ui/CatfishWindow.ui.h:4
msgid "Select Folder"
msgstr "Vælg mappe"

#: ../data/ui/CatfishWindow.ui.h:5
msgid "_Open"
msgstr "_Åbn"

#: ../data/ui/CatfishWindow.ui.h:6
msgid "_Open with..."
msgstr "_Åbn med ..."

#. This menu contains the menu items: _Open, Show in _File Manager, _Copy
#. Location, Save _As, _Delete
#: ../data/ui/CatfishWindow.ui.h:8
msgid "Show in _File Manager"
msgstr "Vis i _filhåndtering"

#. This menu contains the menu items: _Open, Show in _File Manager, _Copy
#. Location, Save _As, _Delete
#: ../data/ui/CatfishWindow.ui.h:10
msgid "_Copy Location"
msgstr "_Kopiér placering"

#: ../data/ui/CatfishWindow.ui.h:11
msgid "_Save as..."
msgstr "_Gem som ..."

#: ../data/ui/CatfishWindow.ui.h:12
msgid "_Delete..."
msgstr "_Slet ..."

#: ../data/ui/CatfishWindow.ui.h:13
msgid "File Extensions"
msgstr "Filendelser"

#: ../data/ui/CatfishWindow.ui.h:14
msgid "Enter a comma-separated list of file extensions (e.g. odt, png, txt)"
msgstr ""
"Indtast en kommasepareret liste over filendelser (f.eks. odt, png, txt)"

#: ../data/ui/CatfishWindow.ui.h:15
msgid "Documents"
msgstr "Dokumenter"

#: ../data/ui/CatfishWindow.ui.h:16
msgid "Folders"
msgstr "Mapper"

#: ../data/ui/CatfishWindow.ui.h:17
msgid "Images"
msgstr "Billeder"

#: ../data/ui/CatfishWindow.ui.h:18
msgid "Music"
msgstr "Musik"

#: ../data/ui/CatfishWindow.ui.h:19
msgid "Videos"
msgstr "Videoer"

#: ../data/ui/CatfishWindow.ui.h:20
msgid "Applications"
msgstr "Programmer"

#: ../data/ui/CatfishWindow.ui.h:21
msgid "Archives"
msgstr "Arkiver"

#: ../data/ui/CatfishWindow.ui.h:22
msgid "Other"
msgstr "Andet"

#: ../data/ui/CatfishWindow.ui.h:23
msgid "Extension..."
msgstr "Endelse ..."

#: ../data/ui/CatfishWindow.ui.h:24
msgid "Any time"
msgstr "Enhver tid"

#: ../data/ui/CatfishWindow.ui.h:25 ../catfish/CatfishWindow.py:1902
msgid "Today"
msgstr "I dag"

#: ../data/ui/CatfishWindow.ui.h:26
msgid "This week"
msgstr "Denne uge"

#: ../data/ui/CatfishWindow.ui.h:27
msgid "This month"
msgstr "Denne måned"

#: ../data/ui/CatfishWindow.ui.h:28
msgid "Custom..."
msgstr "Tilpasset ..."

#: ../data/ui/CatfishWindow.ui.h:29
msgid "Go to Today"
msgstr "Gå til dagen i dag"

#: ../data/ui/CatfishWindow.ui.h:30
msgid "<b>Start Date</b>"
msgstr "<b>Startdato</b>"

#: ../data/ui/CatfishWindow.ui.h:31
msgid "<b>End Date</b>"
msgstr "<b>Slutdato</b>"

#: ../data/ui/CatfishWindow.ui.h:32 ../catfish_lib/Window.py:240
msgid "Catfish"
msgstr "Catfish"

#: ../data/ui/CatfishWindow.ui.h:33
msgid "Update"
msgstr "Opdater"

#: ../data/ui/CatfishWindow.ui.h:34
msgid "The search database is more than 7 days old.  Update now?"
msgstr "Søgedatabasen er mere end 7 dage gammel. Opdater nu?"

#: ../data/ui/CatfishWindow.ui.h:35
msgid "File Type"
msgstr "Filtype"

#: ../data/ui/CatfishWindow.ui.h:36 ../catfish/CatfishWindow.py:1617
msgid "Modified"
msgstr "Ændret"

#: ../data/ui/CatfishWindow.ui.h:38 ../catfish/CatfishWindow.py:2222
msgid "Results will be displayed as soon as they are found."
msgstr "Resultater vil blive vist så snart de findes."

#: ../data/ui/CatfishWindow.ui.h:39
msgid "Unlock"
msgstr "Lås op"

#: ../data/ui/CatfishWindow.ui.h:40
msgid "<b>Database:</b>"
msgstr "<b>Database:</b>"

#: ../data/ui/CatfishWindow.ui.h:41
msgid "<b>Updated:</b>"
msgstr "<b>Opdateret:</b>"

#: ../data/ui/CatfishWindow.ui.h:42
msgid "<big><b>Update Search Database</b></big>"
msgstr "<big><b>Opdater søgedatabase</b></big>"

#: ../data/ui/CatfishWindow.ui.h:43
msgid ""
"For faster search results, the search database needs to be refreshed.\n"
"This action requires administrative rights."
msgstr ""
"For at få hurtigere søgeresultater skal søgedatabasen genopfriskes.\n"
"Denne handling kræver administratortilladelser."

#: ../data/ui/CatfishWindow.ui.h:45
msgid "Update Search Database"
msgstr "Opdater søgedatabase"

#: ../data/ui/CatfishWindow.ui.h:46
msgid "Search for files"
msgstr "Søg efter filer"

#: ../data/ui/CatfishWindow.ui.h:47
msgid "Compact List"
msgstr "Kompakt liste"

#: ../data/ui/CatfishWindow.ui.h:48
msgid "Thumbnails"
msgstr "Miniaturer"

#: ../data/ui/CatfishWindow.ui.h:49
msgid "Show _sidebar"
msgstr "Vis _sidelinje"

#: ../data/ui/CatfishWindow.ui.h:50
msgid "Show _hidden files"
msgstr "Vis _skjulte filer"

#: ../data/ui/CatfishWindow.ui.h:51
msgid "Search file _contents"
msgstr "Søg i fil_indhold"

#: ../data/ui/CatfishWindow.ui.h:52
msgid "_Match results exactly"
msgstr "_Match resultater præcist"

#: ../data/ui/CatfishWindow.ui.h:53
msgid "_Refresh search index..."
msgstr "_Opdater søgeindeks ..."

#: ../data/ui/CatfishWindow.ui.h:54
msgid "_Preferences"
msgstr "_Præferencer"

#: ../data/ui/CatfishWindow.ui.h:55
msgid "_About"
msgstr "_Om"

#: ../catfish/__init__.py:39
msgid "Usage: %prog [options] path query"
msgstr "Anvendelse: %prog [tilvalg] sti forespørgsel"

#: ../catfish/__init__.py:44
msgid "Show debug messages (-vv will also debug catfish_lib)"
msgstr "Vis fejlsøgningsmeddelelser (-vv fejlsøger også catfish_lib)"

#: ../catfish/__init__.py:47
msgid "Use large icons"
msgstr "Brug store ikoner"

#: ../catfish/__init__.py:49
msgid "Use thumbnails"
msgstr "Brug miniaturer"

#: ../catfish/__init__.py:51
msgid "Display time in ISO format"
msgstr "Vis tid i ISO-format"

#. Translators: Do not translate PATH, it is a variable.
#: ../catfish/__init__.py:53
msgid "Set the default search path"
msgstr "Indstil standardsøgestien"

#: ../catfish/__init__.py:55
msgid "Perform exact match"
msgstr "Udfør præcist match"

#: ../catfish/__init__.py:57
msgid "Include hidden files"
msgstr "Medtag skjulte filer"

#: ../catfish/__init__.py:59
msgid "Perform fulltext search"
msgstr "Udfør søgning i fuld tekst"

#: ../catfish/__init__.py:61
msgid ""
"If path and query are provided, start searching when the application is "
"displayed."
msgstr ""
"Hvis sti og forespørgsel gives, så start søgning når programmet vises."

#: ../catfish/__init__.py:63
msgid "set a default column to sort by (name|size|path|date|type),(asc|desc)"
msgstr ""
"indstil en standardkolonne til at sortere efter "
"(name|size|path|date|type),(asc|desc)"

#. Translators: this text is displayed next to
#. a filename that is not utf-8 encoded.
#: ../catfish/CatfishWindow.py:108
#, python-format
msgid "%s (invalid encoding)"
msgstr "%s (ugyldig kodning)"

#: ../catfish/CatfishWindow.py:148
msgid "translator-credits"
msgstr ""
"Per Kongstad\n"
"Aputsiak Niels Janussen\n"
"scootergrisen"

#: ../catfish/CatfishWindow.py:298
msgid "Unknown"
msgstr "Ukendt"

#: ../catfish/CatfishWindow.py:302
msgid "Never"
msgstr "Aldrig"

#: ../catfish/CatfishWindow.py:414
#, python-format
msgid ""
"Enter your query above to find your files\n"
"or click the %s icon for more options."
msgstr ""
"Indtast din forespørgsel ovenfor for at finde dine filer\n"
"eller klik på %s-ikonet for flere valgmuligheder."

#: ../catfish/CatfishWindow.py:812
msgid "An error occurred while updating the database."
msgstr "Der opstod en fejl under opdatering af databasen."

#: ../catfish/CatfishWindow.py:814
msgid "Authentication failed."
msgstr "Autentifikation mislykkedes."

#: ../catfish/CatfishWindow.py:820
msgid "Authentication cancelled."
msgstr "Autentifikation annulleret."

#: ../catfish/CatfishWindow.py:826
msgid "Search database updated successfully."
msgstr "Søgedatabase blev opdateret."

#. Update the Cancel button to Close, make it default
#: ../catfish/CatfishWindow.py:887
msgid "Close"
msgstr "Luk"

#. Set the dialog status to running.
#: ../catfish/CatfishWindow.py:901
msgid "Updating..."
msgstr "Opdaterer ..."

#: ../catfish/CatfishWindow.py:936
msgid "Stop Search"
msgstr "Stop søgning"

#: ../catfish/CatfishWindow.py:937
msgid ""
"Search is in progress...\n"
"Press the cancel button or the Escape key to stop."
msgstr ""
"Igangværende søgning ...\n"
"Tryk på Annuller-knappen eller Escape-tasten for at stoppe."

#: ../catfish/CatfishWindow.py:948
msgid "Begin Search"
msgstr "Begynd søgning"

#: ../catfish/CatfishWindow.py:1072
msgid "No folder selected."
msgstr "Ingen mappe valgt."

#: ../catfish/CatfishWindow.py:1077
msgid "Folder not found."
msgstr "Mappe ikke fundet."

#: ../catfish/CatfishWindow.py:1326
#, python-format
msgid "\"%s\" could not be opened."
msgstr "\"%s\" kunne ikke åbnes."

#: ../catfish/CatfishWindow.py:1482
#, python-format
msgid "\"%s\" could not be saved."
msgstr "\"%s\" kunne ikke gemmes."

#: ../catfish/CatfishWindow.py:1499
#, python-format
msgid "\"%s\" could not be deleted."
msgstr "\"%s\" kunne ikke slettes."

#: ../catfish/CatfishWindow.py:1538
#, python-format
msgid "Save \"%s\" as..."
msgstr "Gem \"%s\" som ..."

#: ../catfish/CatfishWindow.py:1573
#, python-format
msgid ""
"Are you sure that you want to \n"
"permanently delete \"%s\"?"
msgstr ""
"Er du sikker på, at du vil \n"
"slette \"%s\" permanent?"

#: ../catfish/CatfishWindow.py:1577
#, python-format
msgid ""
"Are you sure that you want to \n"
"permanently delete the %i selected files?"
msgstr ""
"Er du sikker på, at du vil \n"
"slette de %i valgte filer permanent?"

#: ../catfish/CatfishWindow.py:1580
msgid "If you delete a file, it is permanently lost."
msgstr "Hvis du sletter en fil, er denne tabt permanent."

#: ../catfish/CatfishWindow.py:1614
msgid "Filename"
msgstr "Filnavn"

#: ../catfish/CatfishWindow.py:1615
msgid "Size"
msgstr "Størrelse"

#: ../catfish/CatfishWindow.py:1616
msgid "Location"
msgstr "Placering"

#: ../catfish/CatfishWindow.py:1626
msgid "Preview"
msgstr "Forhåndsvisning"

#: ../catfish/CatfishWindow.py:1633
msgid "Details"
msgstr "Detaljer"

#: ../catfish/CatfishWindow.py:1823
msgid "Open with default applications"
msgstr "Åbn med standardprogrammer"

#: ../catfish/CatfishWindow.py:1839
msgid "Open with"
msgstr "Åbn med"

#: ../catfish/CatfishWindow.py:1904
msgid "Yesterday"
msgstr "I går"

#: ../catfish/CatfishWindow.py:2012
msgid "No files found."
msgstr "Ingen filer fundet."

#: ../catfish/CatfishWindow.py:2014
msgid ""
"Try making your search less specific\n"
"or try another directory."
msgstr ""
"Prøv at gøre din søgning mindre specifik\n"
"eller prøv en anden mappe."

#: ../catfish/CatfishWindow.py:2021
msgid "1 file found."
msgstr "1 fil fundet."

#: ../catfish/CatfishWindow.py:2023
#, python-format
msgid "%i files found."
msgstr "%i filer fundet."

#: ../catfish/CatfishWindow.py:2032 ../catfish/CatfishWindow.py:2035
msgid "bytes"
msgstr "bytes"

#: ../catfish/CatfishWindow.py:2220 ../catfish/CatfishWindow.py:2230
msgid "Searching..."
msgstr "Søger ..."

#: ../catfish/CatfishWindow.py:2228
#, python-format
msgid "Searching for \"%s\""
msgstr "Søger efter \"%s\""

#: ../catfish/CatfishWindow.py:2338
#, python-format
msgid "Search results for \"%s\""
msgstr "Søgeresultater for \"%s\""

#: ../catfish_lib/catfishconfig.py:94
msgid "Catfish is a versatile file searching tool."
msgstr "Catfish er et alsidigt filsøgningsværktøj."

#: ../catfish_lib/SudoDialog.py:126
msgid "Password Required"
msgstr "Adgangskode krævet"

#: ../catfish_lib/SudoDialog.py:163
msgid "Incorrect password... try again."
msgstr "Forkert adgangskode ... prøv igen."

#: ../catfish_lib/SudoDialog.py:173
msgid "Password:"
msgstr "Adgangskode:"

#: ../catfish_lib/SudoDialog.py:187
msgid "OK"
msgstr "OK"

#: ../catfish_lib/SudoDialog.py:205
msgid ""
"Enter your password to\n"
"perform administrative tasks."
msgstr ""
"Indtast din adgangskode for at\n"
"udføre administrative opgaver."

#: ../catfish_lib/SudoDialog.py:207
#, python-format
msgid ""
"The application '%s' lets you\n"
"modify essential parts of your system."
msgstr ""
"Programmet '%s' lader dig\n"
"ændre essentielle dele af dit system."

#: ../data/metainfo/catfish.appdata.xml.in.h:1
msgid "Versatile file searching tool"
msgstr "Alsidigt filsøgningsværktøj"

#: ../data/metainfo/catfish.appdata.xml.in.h:2
msgid ""
"Catfish is a small, fast, and powerful file search utility. Featuring a "
"minimal interface with an emphasis on results, it helps users find the files"
" they need without a file manager. With powerful filters such as "
"modification date, file type, and file contents, users will no longer be "
"dependent on the file manager or organization skills."
msgstr ""
"Catfish er et lille, hurtigt og kraftfuldt filsøgningsredskab. Med en "
"minimal grænseflade med vægt på resultater hjælper den brugere med at finde "
"de filer de har brug for uden en filhåndtering. Med kraftfulde filtre såsom "
"ændringsdato, filtype og filindhold, behøver brugere ikke længere at være "
"afhængig af filhåndteringen eller organiseringsværdigheder."

#: ../data/metainfo/catfish.appdata.xml.in.h:3
msgid "The main Catfish window displaying image search results"
msgstr "Det primære Catfish-vindue som viser resultater for billedsøgning"

#: ../data/metainfo/catfish.appdata.xml.in.h:4
msgid ""
"The main Catfish window displaying available filters and filtered search "
"results"
msgstr ""
"Det primære Catfish-vindue som viser tilgængelige filtre og filtrerede "
"søgeresultater"

#: ../data/metainfo/catfish.appdata.xml.in.h:5
msgid ""
"This release improves keyboard shortcuts, desktop environment integration, "
"file manager support, and general look and feel. It also features several "
"bug fixes for non-Xfce users."
msgstr ""
"Denne udgivelse forbedrer tastaturgenveje, integration af skrivebordsmiljø, "
"understøttelse af filhåndtering og udseende og fremtoning. Den har også "
"mange fejlrettelser til ikke-Xfce-brugere."

#: ../data/metainfo/catfish.appdata.xml.in.h:6
msgid ""
"This release improves file manager support, zip file support, and handling "
"selected files. It also improves keyboard shortcuts for starting a search or"
" selecting a new search path."
msgstr ""
"Denne udgivelse forbedrer understøttelse af filhåndtering, understøttelse af"
" zip-filer og håndtering af valgte filer. Den forbedrer også tastaturgenveje"
" til at starte en søgning eller vælge en ny søgesti."

#: ../data/metainfo/catfish.appdata.xml.in.h:7
msgid ""
"This release expands fulltext search to support more file types and adds "
"support for searching within compressed files. Searching file contents and "
"the preferred view is now preserved across sessions."
msgstr ""
"Denne udgivelse udvider fuldtekstsøgning til at understøtte flere filtyper "
"og tilføjer understøttelse af søgning i komprimerede filer. Søgning i "
"filindhold og den foretrukne visning bevares nu mellem sessioner."

#: ../data/metainfo/catfish.appdata.xml.in.h:8
msgid ""
"This release expands fulltext search to support more encodings and match all"
" keywords instead of matching any."
msgstr ""
"Denne udgivelse udvider fuldtekstsøgning til at understøtte flere kodninger "
"og matche alle nøgleord frem for nogle."

#: ../data/metainfo/catfish.appdata.xml.in.h:9
msgid ""
"This release better integrates with the Xfce desktop, better supports the "
"Zeitgeist datahub, and includes many bugfixes and usability improvements."
msgstr ""
"Denne udgivelse integrerer bedre med Xfce-skrivebordet, understøtter bedre "
"Zeitgeist-datahubben og inkluderer mange fejlrettelser og forbedringer af "
"anvendeligheden."

#: ../data/metainfo/catfish.appdata.xml.in.h:10
msgid ""
"This release resolves packaging errors for Debian and includes optimized "
"assets."
msgstr ""
"Denne udgivelse løser fejl ved pakning for Debian og medtager optimerede "
"\"assets\"."

#: ../data/metainfo/catfish.appdata.xml.in.h:11
msgid ""
"This release adds support for running under Wayland and features refreshed "
"dialogs with simplified controls."
msgstr ""
"Denne udgivelse tilføjer understøttelse af kørsel under Wayland og har "
"fornyede dialoger med forenklet styringer."

#: ../data/metainfo/catfish.appdata.xml.in.h:12
msgid ""
"This release addresses some startup errors as well as various issues with "
"traversing symbolic links."
msgstr ""
"Denne udgivelse adresserer opstartsproblemer samt diverse problemer i "
"forbindelse med besøg af symbolske links."

#: ../data/metainfo/catfish.appdata.xml.in.h:13
msgid ""
"This release includes many appearance improvements, introduces a new "
"preferences dialog, and improves search results and performance."
msgstr ""
"Denne udgivelse inkluderer mange forbedringer til udseendet, introducerer en"
" ny præferencer-dialog og forbedrer søgeresultater og ydelse."

#: ../data/metainfo/catfish.appdata.xml.in.h:14
msgid ""
"This release features performance improvements by excluding uncommon search "
"directories."
msgstr ""
"Denne udgivelse byder på forbedringer til ydelsen ved at undlade søgemapper "
"som normalt ikke bruges."

#: ../data/metainfo/catfish.appdata.xml.in.h:15
msgid ""
"This release features better desktop integration, support for OpenBSD and "
"Wayland, and improved translation support."
msgstr ""
"Denne udgivelse byder på bedre skrivebordsintegration, understøttelse af "
"OpenBSD og Wayland, og forbedret understøttelse af oversættelse."

#: ../data/metainfo/catfish.appdata.xml.in.h:16
msgid ""
"This release features improved performance, better desktop integration, and "
"a number of long-standing bugs. Quoted search strings are now correctly "
"processed. Files can be dragged into other applications. Thumbnails are only"
" generated when requested by the user."
msgstr ""
"Denne udgivelse byder på forbedret ydelse, bedre skrivebordsintegration og "
"et antal længerevarende fejl. Søgestrenge i citationstegn behandles nu "
"korrekt. Filer kan trækkes ind i andre programmer. Miniaturer genereres kun "
"når det anmodes af brugeren."

#: ../data/metainfo/catfish.appdata.xml.in.h:17
msgid ""
"This release features several improvements to thumbnail processing and "
"numerous bug fixes. Icon previews have been improved and will now match "
"other applications. Items displayed at the bottom of the results window are "
"now accessible with all desktop themes."
msgstr ""
"Denne udgivelse byder på mange forbedringer til behandling af miniature og "
"mange fejlrettelser. Forhåndsvisning af ikoner er blevet forbedret og "
"matcher nu andre programmer. Punkter som vises nederst i resultatsvinduet og"
" kan nu tilgås med alle skrivebordstemaer."

#: ../data/metainfo/catfish.appdata.xml.in.h:18
msgid "This is a minor translations-only release."
msgstr "Dette er en mindre udgivelse som kun har med oversættelser at gøre."

#: ../data/metainfo/catfish.appdata.xml.in.h:19
msgid ""
"This release features several performance improvements and numerous "
"translation updates."
msgstr ""
"Denne udgivelse byder på mange forbedringer til ydelsen og opdateringer af "
"oversættelser."

#: ../data/metainfo/catfish.appdata.xml.in.h:20
msgid ""
"This release now displays all file timestamps according to timezone instead "
"of Universal Coordinated Time (UTC)."
msgstr ""
"Denne udgivelse viser nu alle fil-tidsstempler i henhold til tidszone i "
"stedet for Universal Coordinated Time (UTC)."

#: ../data/metainfo/catfish.appdata.xml.in.h:21
msgid ""
"This release fixes several bugs related to the results window. Files are "
"once again removed from the results list when deleted. Middle- and right-"
"click functionality has been restored. Date range filters are now applied "
"according to timezone instead of Universal Coordinated Time (UTC)."
msgstr ""
"Denne udgivelse retter mange fejl i forbindelse med resultater-vinduet. "
"Filer fjernes nu igen fra resultatslisten når de slettes. Midter- og "
"højreklik er tilbage. Datoområdefiltre anvendes nu i henhold til tidszone i "
"stedet for Universal Coordinated Time (UTC)."

#: ../data/metainfo/catfish.appdata.xml.in.h:22
msgid ""
"This release includes a significant interface refresh, improves search "
"speed, and fixes several bugs. The workflow has been improved, utilizing "
"some of the latest features of the GTK+ toolkit, including optional "
"headerbars and popover widgets. Password handling has been improved with the"
" integration of PolicyKit when available."
msgstr ""
"Denne udgivelse inkluderer en betydelig genopfriskning af grænsefladen, "
"forbedre søgehastighed og rettelse af mange fejl. Arbejdsgangen er blevet "
"forbedret, som gør brug af nogen af de seneste faciliteter i "
"GTK+-toolkittet, inklusiv valgfrie headerlinjer og popover-widgets. "
"Adgangskodehåndtering er blevet forbedret med integreringen af PolicyKit når"
" det er tilgængeligt."

#: ../data/metainfo/catfish.appdata.xml.in.h:23
msgid "This release fixes two new bugs and includes updated translations."
msgstr ""
"Denne udgivelse retter to fejl og inkluderer opdaterede oversættelser."

#: ../data/metainfo/catfish.appdata.xml.in.h:24
msgid ""
"This release fixes a regression where the application is unable to start on "
"some systems."
msgstr ""
"Denne udgivelse retter en regression hvor programmet ikke er i stand til at "
"starte på nogle systemer."

#: ../data/metainfo/catfish.appdata.xml.in.h:25
msgid ""
"This release fixes a regression where multiple search terms were no longer "
"supported. An InfoBar is now displayed when the search database is outdated,"
" and the dialogs used to update the database have been improved."
msgstr ""
"Denne udgivelse retter en regression hvor flere søgetermer ikke længere var "
"understøttet. En infolinje vises nu når søgedatabasen er forældet og "
"dialogerne som bruges til at opdatere databasen er blevet forbedret."

#: ../data/metainfo/catfish.appdata.xml.in.h:26
msgid ""
"This release fixes two issues where locate would not be properly executed "
"and improves handling of missing symbolic icons."
msgstr ""
"Denne udgivelse retter to problemer hvor lokaliser ikke korrekt ville køre "
"og forbedrer handling af manglende symbolske ikoner."

#: ../data/metainfo/catfish.appdata.xml.in.h:27
msgid ""
"This stable release improved the reliability of the password dialog, cleaned"
" up unused code, and fixed potential issues with the list and item "
"selection."
msgstr ""
"Denne stabile udgivelse forbedrede pålideligheden af adgangskode-dialogen, "
"ryddede op i ubrugt kode og rettede potentielle problemer med listen og valg"
" af punkt."

#: ../data/metainfo/catfish.appdata.xml.in.h:28
msgid ""
"This release fixed a potential security issue with program startup and fixed"
" a regression with selecting multiple items."
msgstr ""
"Denne udgivelse rettede et potentielt sikkerhedsproblem med programopstart "
"og rettede en regression med valg af flere punkter."

#: ../data/metainfo/catfish.appdata.xml.in.h:29
msgid ""
"The first release in the 1.0.x series introduced a refreshed interface and "
"fixed a number of long-standing bugs. Improvements to the default "
"permissions eliminated a number of warnings when packaging for "
"distributions. Accessibility was enhanced as all strings have been made "
"translatable and keyboard accelerators have been improved."
msgstr ""
"Den første udgivelse i 1.0.x-serien introducerede en genopfrisket "
"grænseflade og rettede et antal fejl som har været der i lang tid. "
"Forbedringer til standardtilladelser eliminerer et antal advarsler ved "
"pakning af distributioner. Tilgængelighed blev forbedret eftersom alle "
"strenge nu kan oversættes og tastaturgenveje er blevet forbedret."
