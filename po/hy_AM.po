# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Real School <localization@ehayq.am>, 2023
# Hayk Andreasyan <hayk.andreasyan@realschool.am>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-28 00:48+0200\n"
"PO-Revision-Date: 2018-06-28 22:08+0000\n"
"Last-Translator: Hayk Andreasyan <hayk.andreasyan@realschool.am>, 2023\n"
"Language-Team: Armenian (Armenia) (https://app.transifex.com/xfce/teams/16840/hy_AM/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hy_AM\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../org.xfce.Catfish.desktop.in.h:1 ../data/ui/CatfishWindow.ui.h:37
#: ../catfish_lib/catfishconfig.py:91
msgid "Catfish File Search"
msgstr "Catfish նիշերի որոնում"

#: ../org.xfce.Catfish.desktop.in.h:2
msgid "File search"
msgstr "Նիշերի որոնում"

#: ../org.xfce.Catfish.desktop.in.h:3
msgid "Search the file system"
msgstr "Որոնել նիշային համակարգում"

#. TRANSLATORS: Search terms to find this application. Do NOT translate or
#. localize the semicolons! The list MUST also end with a semicolon!
#: ../org.xfce.Catfish.desktop.in.h:5
msgid "files;find;locate;lookup;search;"
msgstr "նիշք;գտնել;բացայայտել;զննել;որոնել;"

#: ../data/ui/CatfishPreferences.ui.h:1
msgid "Catfish Preferences"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:2
msgid "_Close"
msgstr "_Փակել"

#: ../data/ui/CatfishPreferences.ui.h:3
msgid "Classic (_Titlebar)"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:4
msgid "_Modern (CSD)"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:5
msgid "Your new window layout will be applied after restarting Catfish."
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:6
msgid "Window Layout"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:7
msgid "Show _hidden files in the results"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:8
msgid "Show filter _sidebar"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:9
msgid "Show file sizes in binary format"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:10
msgid "Uncheck to show file size in decimal format"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:11
msgid "Display Options"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:12
msgid "Appearance"
msgstr "Տեսք"

#: ../data/ui/CatfishPreferences.ui.h:13
msgid "Path"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:14
msgid "Add Directory..."
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:15
msgid "_Add"
msgstr "_Յաւելել"

#: ../data/ui/CatfishPreferences.ui.h:16
msgid "Remove Directory"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:17
msgid "_Remove"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:18
msgid "Exclude Directories"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:19
msgid "Close the search _window after opening a file"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:20
msgid "Search within compressed files (.zip, .odt, .docx)"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:21
msgid "Miscellaneous"
msgstr ""

#: ../data/ui/CatfishPreferences.ui.h:22
msgid "Advanced"
msgstr "Բարելաւուած"

#: ../data/ui/CatfishWindow.ui.h:1
msgid "Select a Folder"
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:2
msgid "Folder not found!"
msgstr ""

#. Restore Cancel button
#. Buttons
#: ../data/ui/CatfishWindow.ui.h:3 ../catfish/CatfishWindow.py:797
#: ../catfish_lib/SudoDialog.py:184
msgid "Cancel"
msgstr "Չեղարկել"

#: ../data/ui/CatfishWindow.ui.h:4
msgid "Select Folder"
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:5
msgid "_Open"
msgstr "_Բացել"

#: ../data/ui/CatfishWindow.ui.h:6
msgid "_Open with..."
msgstr ""

#. This menu contains the menu items: _Open, Show in _File Manager, _Copy
#. Location, Save _As, _Delete
#: ../data/ui/CatfishWindow.ui.h:8
msgid "Show in _File Manager"
msgstr "Ցուցադրել _նիշքում"

#. This menu contains the menu items: _Open, Show in _File Manager, _Copy
#. Location, Save _As, _Delete
#: ../data/ui/CatfishWindow.ui.h:10
msgid "_Copy Location"
msgstr "_Պատճէնել տեղադրութիւնը"

#: ../data/ui/CatfishWindow.ui.h:11
msgid "_Save as..."
msgstr "_Պահել որպէս․․․"

#: ../data/ui/CatfishWindow.ui.h:12
msgid "_Delete..."
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:13
msgid "File Extensions"
msgstr "Նիշերի ընդլայնումներ"

#: ../data/ui/CatfishWindow.ui.h:14
msgid "Enter a comma-separated list of file extensions (e.g. odt, png, txt)"
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:15
msgid "Documents"
msgstr "Փաստաթղթեր"

#: ../data/ui/CatfishWindow.ui.h:16
msgid "Folders"
msgstr "Պանակներ"

#: ../data/ui/CatfishWindow.ui.h:17
msgid "Images"
msgstr "Պատկերներ"

#: ../data/ui/CatfishWindow.ui.h:18
msgid "Music"
msgstr "Երաժշտութիւն"

#: ../data/ui/CatfishWindow.ui.h:19
msgid "Videos"
msgstr "Տեսանիւթեր"

#: ../data/ui/CatfishWindow.ui.h:20
msgid "Applications"
msgstr "Յաւելուածներ"

#: ../data/ui/CatfishWindow.ui.h:21
msgid "Archives"
msgstr "Պահոցներ"

#: ../data/ui/CatfishWindow.ui.h:22
msgid "Other"
msgstr "Այլ"

#: ../data/ui/CatfishWindow.ui.h:23
msgid "Extension..."
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:24
msgid "Any time"
msgstr "Ցանկացած ժամանակ"

#: ../data/ui/CatfishWindow.ui.h:25 ../catfish/CatfishWindow.py:1902
msgid "Today"
msgstr "Այսաւր"

#: ../data/ui/CatfishWindow.ui.h:26
msgid "This week"
msgstr "Այս շաբաթ"

#: ../data/ui/CatfishWindow.ui.h:27
msgid "This month"
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:28
msgid "Custom..."
msgstr "Ընտրովի․․․"

#: ../data/ui/CatfishWindow.ui.h:29
msgid "Go to Today"
msgstr "Այսաւր"

#: ../data/ui/CatfishWindow.ui.h:30
msgid "<b>Start Date</b>"
msgstr "<b>մեկնարկ ամսաթիւ</b>"

#: ../data/ui/CatfishWindow.ui.h:31
msgid "<b>End Date</b>"
msgstr "<b>Աւարտի ամսաթիւ</b>"

#: ../data/ui/CatfishWindow.ui.h:32 ../catfish_lib/Window.py:240
msgid "Catfish"
msgstr "Catfish"

#: ../data/ui/CatfishWindow.ui.h:33
msgid "Update"
msgstr "Թարմացնել"

#: ../data/ui/CatfishWindow.ui.h:34
msgid "The search database is more than 7 days old.  Update now?"
msgstr "Որոնման շտեմարան աւելի քան 7 աւրուայ է։ Թարմացնե՞լ։"

#: ../data/ui/CatfishWindow.ui.h:35
msgid "File Type"
msgstr "Նիշի տիպ"

#: ../data/ui/CatfishWindow.ui.h:36 ../catfish/CatfishWindow.py:1617
msgid "Modified"
msgstr "Փոփոխուած"

#: ../data/ui/CatfishWindow.ui.h:38 ../catfish/CatfishWindow.py:2222
msgid "Results will be displayed as soon as they are found."
msgstr "Արդիւնքը կը ցուցադրուի, երբ այն գտնուի։"

#: ../data/ui/CatfishWindow.ui.h:39
msgid "Unlock"
msgstr "Արգելաբացել"

#: ../data/ui/CatfishWindow.ui.h:40
msgid "<b>Database:</b>"
msgstr "<b>Շտեմարան․</b>"

#: ../data/ui/CatfishWindow.ui.h:41
msgid "<b>Updated:</b>"
msgstr "<b>Թարմացուած․</b>"

#: ../data/ui/CatfishWindow.ui.h:42
msgid "<big><b>Update Search Database</b></big>"
msgstr "<big><b>Թարմացնել որոնման շտեմարանը</b></big>"

#: ../data/ui/CatfishWindow.ui.h:43
msgid ""
"For faster search results, the search database needs to be refreshed.\n"
"This action requires administrative rights."
msgstr ""
"Աւելի արագ որոնման համար, թարմացրէք շտեմարանը։\n"
"Հարկաւոր է վարիչի արտաւնութիւններ։"

#: ../data/ui/CatfishWindow.ui.h:45
msgid "Update Search Database"
msgstr "Թարմացնել որոնման շտեմարանը"

#: ../data/ui/CatfishWindow.ui.h:46
msgid "Search for files"
msgstr "Որոնել նիշեր"

#: ../data/ui/CatfishWindow.ui.h:47
msgid "Compact List"
msgstr "Սեղմ ցուցակ"

#: ../data/ui/CatfishWindow.ui.h:48
msgid "Thumbnails"
msgstr "Փոքրապատկերներ"

#: ../data/ui/CatfishWindow.ui.h:49
msgid "Show _sidebar"
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:50
msgid "Show _hidden files"
msgstr "Ցուցադրել _թաքնուած նիշերը"

#: ../data/ui/CatfishWindow.ui.h:51
msgid "Search file _contents"
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:52
msgid "_Match results exactly"
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:53
msgid "_Refresh search index..."
msgstr ""

#: ../data/ui/CatfishWindow.ui.h:54
msgid "_Preferences"
msgstr "_Կարգաւորումներ"

#: ../data/ui/CatfishWindow.ui.h:55
msgid "_About"
msgstr "_Ծրագրի մասին"

#: ../catfish/__init__.py:39
msgid "Usage: %prog [options] path query"
msgstr "Գործածում․ %prog [options] path query"

#: ../catfish/__init__.py:44
msgid "Show debug messages (-vv will also debug catfish_lib)"
msgstr ""
"Ցուցադրել վրիպազերծման հաղորդագրութիւննրը (--vv֊ն կը վրիպազերծի նաեւ "
"catfish_lib֊ը)"

#: ../catfish/__init__.py:47
msgid "Use large icons"
msgstr "Գործածել մեծ պատկերակներ"

#: ../catfish/__init__.py:49
msgid "Use thumbnails"
msgstr "Գործածել փոքրապատկերներ"

#: ../catfish/__init__.py:51
msgid "Display time in ISO format"
msgstr "Ժամը՝ ISO ձեւաչափում"

#. Translators: Do not translate PATH, it is a variable.
#: ../catfish/__init__.py:53
msgid "Set the default search path"
msgstr "Սահմանել որոնման լռելեայն ուղին"

#: ../catfish/__init__.py:55
msgid "Perform exact match"
msgstr "Կատարել ճշգրիտ որոնում"

#: ../catfish/__init__.py:57
msgid "Include hidden files"
msgstr "Նեառեալ թաքնուած նիշերը"

#: ../catfish/__init__.py:59
msgid "Perform fulltext search"
msgstr "Կատարել ամբողջագրոյթային որոնում"

#: ../catfish/__init__.py:61
msgid ""
"If path and query are provided, start searching when the application is "
"displayed."
msgstr ""
"Եթե ուղին եւ հարցումը տրամադրուում են, որոնում սկսուում է, երբ յաւելուածը "
"ցուցադրուում է։"

#: ../catfish/__init__.py:63
msgid "set a default column to sort by (name|size|path|date|type),(asc|desc)"
msgstr ""

#. Translators: this text is displayed next to
#. a filename that is not utf-8 encoded.
#: ../catfish/CatfishWindow.py:108
#, python-format
msgid "%s (invalid encoding)"
msgstr "%s(անվաւեր այլագրում)"

#: ../catfish/CatfishWindow.py:148
msgid "translator-credits"
msgstr "E-Hayq LLC, 2019"

#: ../catfish/CatfishWindow.py:298
msgid "Unknown"
msgstr "Անյայտ"

#: ../catfish/CatfishWindow.py:302
msgid "Never"
msgstr "Երբեք"

#: ../catfish/CatfishWindow.py:414
#, python-format
msgid ""
"Enter your query above to find your files\n"
"or click the %s icon for more options."
msgstr ""

#: ../catfish/CatfishWindow.py:812
msgid "An error occurred while updating the database."
msgstr "Շտեմարանի թարմացման սխալ։"

#: ../catfish/CatfishWindow.py:814
msgid "Authentication failed."
msgstr "Վաւերացման ձախողում։"

#: ../catfish/CatfishWindow.py:820
msgid "Authentication cancelled."
msgstr "Վաւերացման չեղարկում։"

#: ../catfish/CatfishWindow.py:826
msgid "Search database updated successfully."
msgstr "Շտեմարանի թարմացումը կատարուած է։"

#. Update the Cancel button to Close, make it default
#: ../catfish/CatfishWindow.py:887
msgid "Close"
msgstr "Փակել"

#. Set the dialog status to running.
#: ../catfish/CatfishWindow.py:901
msgid "Updating..."
msgstr "Թարմացում․․․"

#: ../catfish/CatfishWindow.py:936
msgid "Stop Search"
msgstr "Դադարեցնել"

#: ../catfish/CatfishWindow.py:937
msgid ""
"Search is in progress...\n"
"Press the cancel button or the Escape key to stop."
msgstr ""
"Որոնումն ընթացքի մէջ է․․․\n"
"Սեղմէք չեղարկման կոճակը կամ Escape ստեղնը դադարի համար։"

#: ../catfish/CatfishWindow.py:948
msgid "Begin Search"
msgstr "Սկսել"

#: ../catfish/CatfishWindow.py:1072
msgid "No folder selected."
msgstr ""

#: ../catfish/CatfishWindow.py:1077
msgid "Folder not found."
msgstr ""

#: ../catfish/CatfishWindow.py:1326
#, python-format
msgid "\"%s\" could not be opened."
msgstr "Անհնար է բացել %s֊ը։"

#: ../catfish/CatfishWindow.py:1482
#, python-format
msgid "\"%s\" could not be saved."
msgstr "Անհնար է պահել %s֊ը։"

#: ../catfish/CatfishWindow.py:1499
#, python-format
msgid "\"%s\" could not be deleted."
msgstr "Անհնար է ջնջել %s֊ը։"

#: ../catfish/CatfishWindow.py:1538
#, python-format
msgid "Save \"%s\" as..."
msgstr "Պահել %s֊ը որպէս․․․"

#: ../catfish/CatfishWindow.py:1573
#, python-format
msgid ""
"Are you sure that you want to \n"
"permanently delete \"%s\"?"
msgstr ""
"Ջնջե՞լ\n"
"%s֊ն անվերադարձ։"

#: ../catfish/CatfishWindow.py:1577
#, python-format
msgid ""
"Are you sure that you want to \n"
"permanently delete the %i selected files?"
msgstr ""
"Ջնջե՞լ\n"
"նշուած %i նիշերը։"

#: ../catfish/CatfishWindow.py:1580
msgid "If you delete a file, it is permanently lost."
msgstr "Նիշերը ջնջուում են անվերադարձ։"

#: ../catfish/CatfishWindow.py:1614
msgid "Filename"
msgstr "Նշանուն"

#: ../catfish/CatfishWindow.py:1615
msgid "Size"
msgstr "Չափս"

#: ../catfish/CatfishWindow.py:1616
msgid "Location"
msgstr "Տեղադրութիւն"

#: ../catfish/CatfishWindow.py:1626
msgid "Preview"
msgstr "Նախադիտում"

#: ../catfish/CatfishWindow.py:1633
msgid "Details"
msgstr "Մանրամասնութիւններ"

#: ../catfish/CatfishWindow.py:1823
msgid "Open with default applications"
msgstr ""

#: ../catfish/CatfishWindow.py:1839
msgid "Open with"
msgstr ""

#: ../catfish/CatfishWindow.py:1904
msgid "Yesterday"
msgstr "Երեկ"

#: ../catfish/CatfishWindow.py:2012
msgid "No files found."
msgstr "Նիշերը չեն գտնուել։"

#: ../catfish/CatfishWindow.py:2014
msgid ""
"Try making your search less specific\n"
"or try another directory."
msgstr ""
"Փորձէք կատարել ոչ այդքան յատուկ\n"
"որոնում կամ փորձէք այլ նշարանում։"

#: ../catfish/CatfishWindow.py:2021
msgid "1 file found."
msgstr "1 նիշ է գտնուել։"

#: ../catfish/CatfishWindow.py:2023
#, python-format
msgid "%i files found."
msgstr "%i նիշերը գտնուած չեն։"

#: ../catfish/CatfishWindow.py:2032 ../catfish/CatfishWindow.py:2035
msgid "bytes"
msgstr "բայթեր"

#: ../catfish/CatfishWindow.py:2220 ../catfish/CatfishWindow.py:2230
msgid "Searching..."
msgstr "Որոնում․․․"

#: ../catfish/CatfishWindow.py:2228
#, python-format
msgid "Searching for \"%s\""
msgstr "Որոնում ենք %s ֊ը"

#: ../catfish/CatfishWindow.py:2338
#, python-format
msgid "Search results for \"%s\""
msgstr "Որոնում ենք արդիւնք %s֊ի համար "

#: ../catfish_lib/catfishconfig.py:94
msgid "Catfish is a versatile file searching tool."
msgstr "Catfish֊ը նիշերի որոնման բազմակողմանի գործիք է"

#: ../catfish_lib/SudoDialog.py:126
msgid "Password Required"
msgstr "Պահանջուում է գախտնաբառ"

#: ../catfish_lib/SudoDialog.py:163
msgid "Incorrect password... try again."
msgstr "Սխալ գախտնաբառ․․․ փորձեք կրկին։"

#: ../catfish_lib/SudoDialog.py:173
msgid "Password:"
msgstr "Գախտնաբառ․"

#: ../catfish_lib/SudoDialog.py:187
msgid "OK"
msgstr "OK"

#: ../catfish_lib/SudoDialog.py:205
msgid ""
"Enter your password to\n"
"perform administrative tasks."
msgstr ""
"Ներմուծեք Ձեր գախտնաբառը\n"
" վարչարարական առաջադրանքները իրագործելու համար։"

#: ../catfish_lib/SudoDialog.py:207
#, python-format
msgid ""
"The application '%s' lets you\n"
"modify essential parts of your system."
msgstr ""
"Յաւելուած \"%s\"֊ը թոյլ է տալիս\n"
"կատարելագործել Ձեր համակարգի ամրագրուած մասերը։"

#: ../data/metainfo/catfish.appdata.xml.in.h:1
msgid "Versatile file searching tool"
msgstr "Նիշերի բազմակողմանի որոնման գործիք"

#: ../data/metainfo/catfish.appdata.xml.in.h:2
msgid ""
"Catfish is a small, fast, and powerful file search utility. Featuring a "
"minimal interface with an emphasis on results, it helps users find the files"
" they need without a file manager. With powerful filters such as "
"modification date, file type, and file contents, users will no longer be "
"dependent on the file manager or organization skills."
msgstr ""
"Catfish֊ը փոքր, արագ եւ հզոր որոնման գործիք է։ Նուազագոյն միջերեսի եւ "
"արդիւնքների ստացման շեշտադրութեամբ, այն թոյլ է տալիս գտնել նիշեր առանց նիշքի"
" միջամտութեան։ Հզոր զտիչների, (փոփոխման ամսաթիւը, նիշի տիպը, "
"բովանդակութիւնը) շնորհիւ աւգտատէրերն այլեւս չեն ունենայ կախուածութիւն "
"նիշային համակարգից։"

#: ../data/metainfo/catfish.appdata.xml.in.h:3
msgid "The main Catfish window displaying image search results"
msgstr "Հիմնական Catfish պատուհանը ցուցադրում է որոնուած արդիւնքների նկարները"

#: ../data/metainfo/catfish.appdata.xml.in.h:4
msgid ""
"The main Catfish window displaying available filters and filtered search "
"results"
msgstr ""
"Հիմնական Catfish պատուհանը ցուցադրում է առկա զտիչները եւ զտուած որոնման "
"արդիւնքները"

#: ../data/metainfo/catfish.appdata.xml.in.h:5
msgid ""
"This release improves keyboard shortcuts, desktop environment integration, "
"file manager support, and general look and feel. It also features several "
"bug fixes for non-Xfce users."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:6
msgid ""
"This release improves file manager support, zip file support, and handling "
"selected files. It also improves keyboard shortcuts for starting a search or"
" selecting a new search path."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:7
msgid ""
"This release expands fulltext search to support more file types and adds "
"support for searching within compressed files. Searching file contents and "
"the preferred view is now preserved across sessions."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:8
msgid ""
"This release expands fulltext search to support more encodings and match all"
" keywords instead of matching any."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:9
msgid ""
"This release better integrates with the Xfce desktop, better supports the "
"Zeitgeist datahub, and includes many bugfixes and usability improvements."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:10
msgid ""
"This release resolves packaging errors for Debian and includes optimized "
"assets."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:11
msgid ""
"This release adds support for running under Wayland and features refreshed "
"dialogs with simplified controls."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:12
msgid ""
"This release addresses some startup errors as well as various issues with "
"traversing symbolic links."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:13
msgid ""
"This release includes many appearance improvements, introduces a new "
"preferences dialog, and improves search results and performance."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:14
msgid ""
"This release features performance improvements by excluding uncommon search "
"directories."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:15
msgid ""
"This release features better desktop integration, support for OpenBSD and "
"Wayland, and improved translation support."
msgstr ""

#: ../data/metainfo/catfish.appdata.xml.in.h:16
msgid ""
"This release features improved performance, better desktop integration, and "
"a number of long-standing bugs. Quoted search strings are now correctly "
"processed. Files can be dragged into other applications. Thumbnails are only"
" generated when requested by the user."
msgstr ""
"Տարբեակում հզորացուած է միջերեսը, աշխատասեղանի ամբողջացումը, լուծուած են "
"որոշ խնդիրներ։ Չակերտաւորուած տողերի որոնման պատրաստումը ներկայումս ընթացքի "
"մէջ է։ Նիշերի կարելի է տեղադրել այլ յաւելուածներում։ Փոքրապատկերները "
"ստեղծուուում են միայն ըստ պահանջի։"

#: ../data/metainfo/catfish.appdata.xml.in.h:17
msgid ""
"This release features several improvements to thumbnail processing and "
"numerous bug fixes. Icon previews have been improved and will now match "
"other applications. Items displayed at the bottom of the results window are "
"now accessible with all desktop themes."
msgstr ""
"Տարբերակում մշակուած է փոքրապատկերների հետ աշխատանքը եւ լուծուած են որոշ "
"խնդիրներ։ Պատկերակների ցուցադրումը հզորացուած է եւ չի համապատասխանի այլ "
"յաւելուածների հետ։ Արդիւնքի պատուհանում ցուցադրուող տարրերն արդէն հասանելի "
"են աշխատասեղանից։"

#: ../data/metainfo/catfish.appdata.xml.in.h:18
msgid "This is a minor translations-only release."
msgstr "Սա սոսկ թարգմանական տարբերակ է։"

#: ../data/metainfo/catfish.appdata.xml.in.h:19
msgid ""
"This release features several performance improvements and numerous "
"translation updates."
msgstr ""
"Տարբերակում թարմացուած է միջերեսը եւ թարմացուած են որոշ թարգմանութիւններ։"

#: ../data/metainfo/catfish.appdata.xml.in.h:20
msgid ""
"This release now displays all file timestamps according to timezone instead "
"of Universal Coordinated Time (UTC)."
msgstr ""
"Տարբերակում ցուցադրուում է բոլոր ժամակնիքները, ըստ ժամագաւտու այլ ոչ "
"(UTC)֊ի։"

#: ../data/metainfo/catfish.appdata.xml.in.h:21
msgid ""
"This release fixes several bugs related to the results window. Files are "
"once again removed from the results list when deleted. Middle- and right-"
"click functionality has been restored. Date range filters are now applied "
"according to timezone instead of Universal Coordinated Time (UTC)."
msgstr ""
"Տարբերակում ուղղուած են արդիւնքի պատուհանի որոշ սխալներ։ Նիշերը հեռացուում "
"են եւս մեկ անգամ արդիւնքի ցուցակից ջնջուելուց յետոյ։ Վերականգնուել է աջ եւ "
"կենտրոնասեղմումի գործառութիւնը։ Տուեալների ընդգրկոյթային զտիչները "
"կարգաւորուում են ըստ ժամագաւտու այլ ոչ (UTC)֊ի։"

#: ../data/metainfo/catfish.appdata.xml.in.h:22
msgid ""
"This release includes a significant interface refresh, improves search "
"speed, and fixes several bugs. The workflow has been improved, utilizing "
"some of the latest features of the GTK+ toolkit, including optional "
"headerbars and popover widgets. Password handling has been improved with the"
" integration of PolicyKit when available."
msgstr ""
"Տարբերակում թարմացուած է միջերեսը, որոնման արագութիւնը, ուղղուած են որոշ "
"խնդիրներ։ Մշակուել է աշխատահոսքը, որն իր մեջ ներառում GTK+ գործիքակազմի "
"ամենաթարմ յատկութիւնները։ Թարմացուել է գաղտնաբառերի հետ աշխատանքը, որն արդէն"
" ներառում է PolicyKit֊ը։"

#: ../data/metainfo/catfish.appdata.xml.in.h:23
msgid "This release fixes two new bugs and includes updated translations."
msgstr ""
"Տարբերակում ուղղուած են երկու սխալ եւ թարմացուած են թարգմանութիւնները  "

#: ../data/metainfo/catfish.appdata.xml.in.h:24
msgid ""
"This release fixes a regression where the application is unable to start on "
"some systems."
msgstr "Տարբերակում ուղղուած է որոշ համակարգերում յաւելուածի միացման խնդիրը։"

#: ../data/metainfo/catfish.appdata.xml.in.h:25
msgid ""
"This release fixes a regression where multiple search terms were no longer "
"supported. An InfoBar is now displayed when the search database is outdated,"
" and the dialogs used to update the database have been improved."
msgstr ""
"Տարբերակում ուղղուած է բազմակի եզրոյթների որոնման հետ կապուած խնդիրը։ "
"InfoBar֊ը ցուցադրուում է, երբ հնանում է որոնման շտեմարանը, մշակուել է նաեւ "
"շտեմարանի թարմացումը։ "

#: ../data/metainfo/catfish.appdata.xml.in.h:26
msgid ""
"This release fixes two issues where locate would not be properly executed "
"and improves handling of missing symbolic icons."
msgstr ""
"Տարբերակում ուղղուած է երկու խնդիր կապուած որոնման գործողութեան ճիշտ "
"գործարկման եւ նշանային պատկերակների բացակայութեան հետ կապուած։"

#: ../data/metainfo/catfish.appdata.xml.in.h:27
msgid ""
"This stable release improved the reliability of the password dialog, cleaned"
" up unused code, and fixed potential issues with the list and item "
"selection."
msgstr ""
"Այս կայուն տարբերակում գաղտնաբառի պատուհանի հուսանելիութիւնն աւելացել է, "
"մաքրուել է չգործարկուող այլագիրը եւ ուղղուել է ցուցակի ու տարրերի ընտրութեան"
" հետ կապուած խնդիրները։"

#: ../data/metainfo/catfish.appdata.xml.in.h:28
msgid ""
"This release fixed a potential security issue with program startup and fixed"
" a regression with selecting multiple items."
msgstr ""
"Տարբերակում լուծուած են պաշտպանութեան սխալները, ուղղած է հետադարձը կապը "
"ընտրուած բազմակի տարրերի հետ։"

#: ../data/metainfo/catfish.appdata.xml.in.h:29
msgid ""
"The first release in the 1.0.x series introduced a refreshed interface and "
"fixed a number of long-standing bugs. Improvements to the default "
"permissions eliminated a number of warnings when packaging for "
"distributions. Accessibility was enhanced as all strings have been made "
"translatable and keyboard accelerators have been improved."
msgstr ""
"1.0x֊ի առաջին տարբերակում թարմացուել է ծրագրի միջերեսը եւ ուղղուել են որոշ "
"խնդիրներ։ Լռելեայն արտաւնութիւնների փոփոխութիւնների արդիւնքում վերացուել են "
"մի շարք խնդիրներ փաթեթաւորման հետ կապուած։ Տողերի թարգմանելի դառնալու "
"արդիւնքում ընդլայնուել է հասանելիութիւնը, մշակուել են ստեղնաշարի "
"արագացուցիչները։"
